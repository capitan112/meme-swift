//
//  MemeTableViewController.swift
//  MemeMe
//
//  Created by Капитан on 22.03.15.
//  Copyright (c) 2015 com.capitan. All rights reserved.
//

let rowHeight: CGFloat = 120

import UIKit

class MemeTableViewController: UITableViewController {
    
    var memes: [Meme]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let object = UIApplication.sharedApplication().delegate
        let appDelegate = object as AppDelegate
        self.memes = appDelegate.memes
        
        var tblView = UIView(frame: CGRectZero)
        tableView.tableFooterView = tblView
        tableView.tableFooterView?.hidden = true
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memes.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tableMemeCell", forIndexPath: indexPath) as UITableViewCell
        let meme = self.memes[indexPath.row]
        cell.textLabel?.text = meme.topText! + " " + meme.bottomText!
        let photoImageView : UIImageView = self.view.viewWithTag(100) as UIImageView!
        photoImageView.image = meme.memedImage
        photoImageView.contentMode = .ScaleAspectFill
        photoImageView.clipsToBounds = true
//        cell.backgroundView?.contentMode = .ScaleAspectFill
//        cell.backgroundView?.clipsToBounds = true

        
        return cell
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return rowHeight
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath
        indexPath: NSIndexPath) {
        let detailController = self.storyboard!.instantiateViewControllerWithIdentifier("MemeDetailViewController")! as MemeDetailViewController
        detailController.meme = self.memes[indexPath.row]
        self.navigationController!.pushViewController(detailController, animated: true)
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            self.memes.removeAtIndex(indexPath.row)
            let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
            appDelegate.memes = self.memes
            self.tableView.reloadData()
        }
    }
    
    //MARK: meme button handling
    
    @IBAction func addButtonPressed(sender: UIBarButtonItem) {
        let memeController = self.storyboard!.instantiateViewControllerWithIdentifier("MemeViewController")! as MemeViewController
        memeController.hidesBottomBarWhenPushed = true
        self.navigationController!.pushViewController(memeController, animated: false)
    }


}

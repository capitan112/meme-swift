//
//  MemeDetailViewController.swift
//  MemeMe
//
//  Created by Капитан on 23.03.15.
//  Copyright (c) 2015 com.capitan. All rights reserved.
//

import UIKit

class MemeDetailViewController: UIViewController {
    
    @IBOutlet weak var photoImage: UIImageView!
    var meme: Meme!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.photoImage!.image = meme.memedImage
    }
    
}

//
//  ViewController.swift
//  MemeMe
//
//  Created by Капитан on 21.03.15.
//  Copyright (c) 2015 com.capitan. All rights reserved.
//


import UIKit
import Foundation


class MemeViewController: UIViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate, UITextFieldDelegate {
    //MARK: property
    @IBOutlet weak var imagePickerView: UIImageView!
    @IBOutlet weak var cameraButton: UIBarButtonItem!
    @IBOutlet weak var topTextField: UITextField!
    @IBOutlet weak var bottomTextField: UITextField!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var shareButton: UIBarButtonItem!

    var pickerController : UIImagePickerController!
    var meme : Meme!
    var memedImage : UIImage!

    //MARK: base functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerController = UIImagePickerController()
        pickerController.delegate = self
        self.topTextField.delegate = self
        self.bottomTextField.delegate = self
        
        let memeTextAttributes = [

            NSForegroundColorAttributeName : UIColor.whiteColor(),
            NSStrokeColorAttributeName : UIColor.blackColor(),
            NSStrokeWidthAttributeName : -3,
            NSFontAttributeName : UIFont(name: "HelveticaNeue-CondensedBlack", size: 40)!
        ]
        
        self.topTextField.text = "TOP"
        self.bottomTextField.text = "BOTTOM"
        self.topTextField.defaultTextAttributes = memeTextAttributes
        self.bottomTextField.defaultTextAttributes = memeTextAttributes
        self.topTextField.textAlignment = .Center
        self.bottomTextField.textAlignment = .Center
        self.shareButton.enabled = false
    }

    override func viewWillAppear(animated: Bool) {
        self.navigationController?.hidesBottomBarWhenPushed = true
        super.viewWillAppear(animated)
        self.subscribeToKeyboardNotifications()
        cameraButton.enabled = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.unsubscribeFromKeyboardNotifications()
    }

    //MARK: picker
    
    @IBAction func pickAnImage(sender: AnyObject) {
        pickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(pickerController, animated: true, completion: nil)
    }
    
    @IBAction func pickAnImageFromCamera (sender: AnyObject) {
        self.presentViewController(pickerController, animated: true, completion: nil)
        pickerController.sourceType = UIImagePickerControllerSourceType.Camera
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imagePickerView.image = image
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: textField methods
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.text = ""
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if self.topTextField.isFirstResponder() || self.bottomTextField.isFirstResponder() {
            self.shareButton.enabled = true
        } else {
            self.shareButton.enabled = false
        }
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if self.bottomTextField.isFirstResponder() {
            self.view.frame.origin.y -= getKeyboardHeight(notification)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if self.bottomTextField.isFirstResponder() {
            self.view.frame.origin.y += getKeyboardHeight(notification)
        }
    }

    
    func getKeyboardHeight(notification: NSNotification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as NSValue // of CGRect
        
        return keyboardSize.CGRectValue().height
    }
    
    func subscribeToKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:",
                                                                   name: UIKeyboardWillShowNotification,
                                                                 object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:",
                                                                   name: UIKeyboardWillHideNotification,
                                                                 object: nil)
    }
    
    func unsubscribeFromKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, 
                                                                object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification,
                                                                object: nil)
    }
    
    //MARK: meme generator
    
    func save() {
        
        if let originalImage = imagePickerView.image {
            self.meme = Meme(topText: topTextField.text, bottomText: bottomTextField.text,
                                                      originalImage: originalImage,
                                                         memedImage: memedImage)
                
            let object = UIApplication.sharedApplication().delegate
            let appDelegate = object as AppDelegate
            appDelegate.memes.append(meme)
        }
    }

    func generateMemedImage() -> UIImage {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.toolBar.hidden = true

        UIGraphicsBeginImageContext(self.view.frame.size)
        self.view.drawViewHierarchyInRect(self.view.frame, afterScreenUpdates: true)
        let memedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.toolBar.hidden = false
        
        return memedImage
    }
    
    //MARK: meme button handling
    
    @IBAction func shareButtonPressed(sender: UIBarButtonItem) {
        self.memedImage = self.generateMemedImage()

        var sharingItems = [AnyObject]()
        if let image = self.memedImage {
            sharingItems.append(image)
        }
        
        let activityViewController = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        self.presentViewController(activityViewController, animated: true, completion: nil)
        activityViewController.completionWithItemsHandler = myHandler
    }
    
    func myHandler(activityType:String!, completed: Bool,
        returnedItems: [AnyObject]!, error: NSError!) {

        self.save()
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}


